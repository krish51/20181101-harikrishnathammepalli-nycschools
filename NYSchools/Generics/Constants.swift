//
//  Constants.swift
//  NYSchools
//
//  Created by Harikrishna on 10/30/18.
//  Copyright © 2018 Harikrishna. All rights reserved.
//

import UIKit

enum Keys {
    static let DBN = "dbn"
    static let SCHOOL_NAME = "school_name"
    static let MATH_AVG_SCORE = "sat_math_avg_score"
    static let READING_AVG_SCORE = "sat_critical_reading_avg_score"
    static let WRITING_AVG_SCORE = "sat_writing_avg_score"
}

let NYC_HIGH_SCHOOL_URL = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
let SAT_RESULTS = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
