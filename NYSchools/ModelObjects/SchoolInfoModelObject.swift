//
//  SchoolInfoModelObject.swift
//  NYSchools
//
//  Created by Harikrishna on 10/30/18.
//  Copyright © 2018 Harikrishna. All rights reserved.
//

import UIKit

struct SchoolInfoModelObject {
    //Objects
    var schoolID: String?
    var schoolName: String?
}
