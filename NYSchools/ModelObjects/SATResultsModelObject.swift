//
//  SATResultsModelObject.swift
//  NYSchools
//
//  Created by Harikrishna on 10/30/18.
//  Copyright © 2018 Harikrishna. All rights reserved.
//


import UIKit

struct SATResultsModelObject {
    //Objects
    var satResultsID: String?
    var mathAvgScore: String?
    var writingAvgScore: String?
    var readingAvgScore: String?
    
}
