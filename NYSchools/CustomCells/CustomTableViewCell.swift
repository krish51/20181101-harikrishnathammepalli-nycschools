//
//  CustomTableViewCell.swift
//  NYSchools
//
//  Created by Harikrishna on 10/30/18.
//  Copyright © 2018 Harikrishna. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
  //HANDLING CUSTOM CELL PROPERTIES
    var readingLabel: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font  = UIFont .systemFont(ofSize: 20)
        return label
    }()
    
    var writingLabel: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()
    
    var mathLabel: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(mathLabel)
        self.addSubview(readingLabel)
        self.addSubview(writingLabel)
        setUpConstraintsForMathLabel()
        setUpConstraintsForReadingLabel()
        setUpConstraintsForWritingLabel()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

//MARK:- HANDLING CONSTRAINTS

extension CustomTableViewCell {
    
    func setUpConstraintsForMathLabel() {
        
        mathLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        mathLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        mathLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        mathLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
    }
    
    func setUpConstraintsForReadingLabel() {
        
        readingLabel.topAnchor.constraint(equalTo: mathLabel.bottomAnchor, constant: 10).isActive = true
        readingLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        readingLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        readingLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        
    }
    
    func setUpConstraintsForWritingLabel() {
        
        writingLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        writingLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        writingLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        writingLabel.topAnchor.constraint(equalTo: readingLabel.bottomAnchor, constant: 10).isActive = true
        writingLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 10).isActive = true
    }
}
