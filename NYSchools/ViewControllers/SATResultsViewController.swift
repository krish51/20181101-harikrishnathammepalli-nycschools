//
//  SATResultsViewController.swift
//  NYSchools
//
//  Created by Harikrishna on 10/30/18.
//  Copyright © 2018 Harikrishna. All rights reserved.
//

import UIKit

class SATResultsViewController: UITableViewController {
    
//MARK:- PROPERTIES
    
    var schoolId: String?
    var resultsArray = [Any]()
    var activityIndicator: UIActivityIndicatorView?
    
//MARK:- VIEW LIFECYCLE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: CellIdentifiers.customCell)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge, color: .gray, placeInTheCenterOf: self.view)
        activityIndicator?.startAnimating()
        DataParseClass.instance.delegate = self
        DataParseClass.instance.jsonGetMethod(urlString: SAT_RESULTS)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK:- UITABLEVIEW DATASOURCE METHODS
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath:
        IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.customCell, for: indexPath) as! CustomTableViewCell
        if let modelObject = resultsArray[indexPath.row] as? SATResultsModelObject {
            
            if let mathAvgScore = modelObject.mathAvgScore, let readingAvgScore = modelObject.readingAvgScore, let writingAvgScore = modelObject.writingAvgScore {
                
                cell.mathLabel.text    = "Math Avg Score      :   \(mathAvgScore)"
                cell.readingLabel.text = "Reading Avg Score   :   \(readingAvgScore)"
                cell.writingLabel.text = "Writing Avg Score   :   \(writingAvgScore)"
                
            }
        }
        return cell
    }
}

//MARK:- NYSCHOOL PROTOCOL METHOD

extension SATResultsViewController: NYCSchoolProtocol {
    
    func responseSuccess(successData: Any) {
        
        if let satResultsArray = successData as? [Any] {
            var modelObject = SATResultsModelObject()
            var count = 0
            for satResultsDict in satResultsArray {
                
                if let dict = satResultsDict as? [String: Any] {
                    if schoolId == dict[Keys.DBN] as? String {
                        if let mathAvgScore = dict[Keys.MATH_AVG_SCORE] as? String, let readingAvgScore = dict[Keys.READING_AVG_SCORE] as? String, let writingAvgScore = dict[Keys.WRITING_AVG_SCORE] as? String {
                            count = count + 1
                            print(readingAvgScore)
                            print(writingAvgScore)
                            print(mathAvgScore)
                            modelObject.mathAvgScore = mathAvgScore
                            modelObject.readingAvgScore = readingAvgScore
                            modelObject.writingAvgScore = writingAvgScore
                            resultsArray.append(modelObject)
                        }
                    }
                }
            }
            if count == 0{
                DataParseClass.instance.alertMethod(message: "No Data Found")
            }
            activityIndicator?.stopAnimating()
            self.tableView.reloadData()
        }
    }
}

