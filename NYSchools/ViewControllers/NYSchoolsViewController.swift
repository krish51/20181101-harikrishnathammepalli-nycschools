//
//  NYSchoolsViewController.swift
//  NYSchools
//
//  Created by Harikrishna on 10/30/18.
//  Copyright © 2018 Harikrishna. All rights reserved.
//

import UIKit
//MARK:- CELL IDENTIFIERS
enum CellIdentifiers {
    
    static let tableViewCell = "cell"
    static let customCell = "CustomTableViewCell"
}

class NYSchoolsViewController: UIViewController {
    
//MARK:- PROPERTIES
    
    var highSchoolArray = [Any]()
    var activityIndicator: UIActivityIndicatorView = {
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = UIColor(r: 25, g: 56, b: 89)
        return activityIndicator
    }()
    
    var tableView: UITableView = {
        
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    var activicityIndicator: UIActivityIndicatorView?
    
//MARK:- VIEW LIFECYCLE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "NYC School List"
        handleUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//MARK:- HANDLE_UI METHOD
    
    func handleUI() {
        
        self.view.addSubview(tableView)
        setUpConstraintsForTableView()
        self.view.addSubview(activityIndicator)
        setUpConstraintsForActivityIndicator()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.accessibilityIdentifier = "SchoolTable"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifiers.tableViewCell)
        tableView.tableFooterView = UIView()
        activicityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge, color: .gray, placeInTheCenterOf: self.view)
        activicityIndicator?.startAnimating()
        DataParseClass.instance.jsonGetMethod(urlString: NYC_HIGH_SCHOOL_URL)
        DataParseClass.instance.delegate = self
    }
}

//MARK:- UITABLEVIEW DATASOURCE METHODS

extension NYSchoolsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return highSchoolArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:CellIdentifiers.tableViewCell , for: indexPath)
        let modelObject = highSchoolArray[indexPath.row] as? SchoolInfoModelObject
        cell.textLabel?.text = modelObject?.schoolName
        return cell
    }
    
}

//MARK:- UITABLEVIEW DELEGATE METHODS

extension NYSchoolsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath:
        IndexPath) {
        
        let satViewController = SATResultsViewController()
        let modelObject = highSchoolArray[indexPath.row] as? SchoolInfoModelObject
        satViewController.schoolId = modelObject?.schoolID
        navigationController?.pushViewController(satViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}

//MARK:- NYSCHOOLPROTOCOL MEHOD HANDLING

extension NYSchoolsViewController : NYCSchoolProtocol {
    
    func responseSuccess(successData: Any) {
        
        if let schoolArray = successData as? [Any] {
            var modelObject = SchoolInfoModelObject()
            for schoolInfoDict in schoolArray {
                if let dict = schoolInfoDict as? [String: Any] {
                    modelObject.schoolID = dict[Keys.DBN] as? String
                    modelObject.schoolName = dict[Keys.SCHOOL_NAME] as? String
                    highSchoolArray.append(modelObject)
                }
            }
            self.tableView.reloadData()
            activicityIndicator?.stopAnimating()
            activicityIndicator?.hidesWhenStopped = true
        }
    }
}

//MARK:- HANDLING CONSTRAINTS

extension NYSchoolsViewController {
    
    func setUpConstraintsForTableView() {
        
        tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    }
    
    func setUpConstraintsForActivityIndicator() {
        
        activityIndicator.heightAnchor.constraint(equalToConstant: 100).isActive = true
        activityIndicator.widthAnchor.constraint(equalToConstant: 100).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.tableView.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.tableView.centerXAnchor).isActive = true
        
    }
}

//MARK:- UICOLOR EXTENSION

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red:r/255,green:g/255,blue:b/255,alpha:1)
    }
}

//MARK:- UIACTIVITYINDICATORVIEW EXTENSION

extension UIActivityIndicatorView {
    
    convenience init(activityIndicatorStyle: UIActivityIndicatorViewStyle, color: UIColor, placeInTheCenterOf parentView: UIView) {
        self.init(activityIndicatorStyle: activityIndicatorStyle)
        center = parentView.center
        self.color = color
        parentView.addSubview(self)
    }
}


