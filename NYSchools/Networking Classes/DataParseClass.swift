//
//  DataParseClass.swift
//  NYSchools
//
//  Created by Harikrishna on 10/30/18.
//  Copyright © 2018 Harikrishna. All rights reserved.
//

import UIKit

//Error Codes
enum StatusCode {
    static let resposneSuccess = 200
}
//Protocol Methods
protocol NYCSchoolProtocol {
    func responseSuccess(successData: Any)
}

//MARK:- Data Parsing Class to get NYSchools Data

class DataParseClass: NSObject {
    
    var delegate: NYCSchoolProtocol?
    //Pass NYSchools and NYSchools SAT Results JSON URL Strings.
    func jsonGetMethod(urlString: String){
        
        let url = URL(string: urlString)
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            DispatchQueue.main.async {
                if let httpResponse = response as? HTTPURLResponse{
                    
                    if httpResponse.statusCode == StatusCode.resposneSuccess {
                        //HANDLE DATA
                        do {
                             let getJsonNYCSchool = try JSONSerialization.jsonObject(with: data!, options: [])
                                self.delegate?.responseSuccess(successData: getJsonNYCSchool)
                        } catch(let err) {
                            fatalError(err.localizedDescription)
                        }
                    }
                }
            }
            
            }.resume()
    }
    
//MARK:- Alert Method to show the status of Data
    
    func alertMethod(message: String) {
        
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
        }
        
        alert.addAction(alertAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    static let instance = DataParseClass()
}
